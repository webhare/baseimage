#!/bin/sh
cd "${0%/*}" || exit 1   #can't do BASH_SOURCE in sh

set | grep -E '(^CI_|DOCKERHUB_)' | grep -v PASSWORD
set | grep -E '(^CI_|DOCKERHUB_)' | grep PASSWORD | cut -d= -f1

set -e
mkdir -p dropins/opt/container/etc
git rev-parse HEAD > dropins/opt/container/etc/baseimage-version
# CI checkouts break the actual branch reported by git, so in that case we take it from the vars
echo "${CI_COMMIT_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}" > dropins/opt/container/etc/baseimage-branch

if [ -n "$CI_REGISTRY" ]; then
  echo Login to "$CI_REGISTRY"
  echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
  BUILDVERSION="${CI_COMMIT_TAG:-$CI_COMMIT_REF_SLUG}"
  BUILDTAG="$CI_REGISTRY_IMAGE:$BUILDVERSION"

  if [ -n "$DOCKERHUB_REGISTRY_PASSWORD" ]; then
    DOCKERHUBIMAGE="docker.io/webhare/baseimage"
    echo Login to "$DOCKERHUBIMAGE"
    echo "$DOCKERHUB_REGISTRY_PASSWORD" | docker login -u "$DOCKERHUB_REGISTRY_USER" --password-stdin $DOCKERHUBIMAGE
  fi
else
  BUILDVERSION=devbuild
  BUILDTAG="docker.io/webhare/baseimage:$(git branch --show-current)"
fi

docker build --pull --progress plain --tag "$BUILDTAG" .
if [ -n "$CI_REGISTRY" ]; then
 docker push "$BUILDTAG"
fi

if [ -n "$DOCKERHUB_REGISTRY_PASSWORD" ]; then
  docker tag "$BUILDTAG" "$DOCKERHUBIMAGE:$BUILDVERSION"
  docker push "$DOCKERHUBIMAGE:$BUILDVERSION"
fi
