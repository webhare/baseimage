#!/bin/sh
BRANCH="$(git branch --show-current)"
echo "To access the image: docker exec -ti baseimage-$BRANCH /bin/bash"
exec docker run --rm --name "baseimage-$BRANCH" -ti docker.io/webhare/baseimage:"$BRANCH"
