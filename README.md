# webhare/baseimage
This is the base image for other docker containers we build (internally and public)

Inspired by the phusion/baseimage, we take runit as a service runner and
wrap it with a shell script to deal with proper termination signal handling

## Container standards
Minimal Dockerfile
```
# The base image
FROM       webhare/baseimage:ubuntu-20

# If your containers data doesn't live in /data, symlink /data to the actual data volume
RUN        ln -sf /opt/webhare-proxy-data/ /data
```

Special/expected files
- `/opt/container/init.sh` - container specific initialziation
- `/opt/container/services` - runit service dirs
- `/opt/container/checks/selftest-*.sh` - nagios checks. should exit with 1 for WARNING and 2 for ERROR, the highest error code will be returend

## Setup/conversion guide
- Use the minimal dockerfile as a base
- remove `CMD`
- your container startup/init code should be in /opt/container/init.sh
  - if you have a launch.sh, rename it to init.sh and remove runit invocation
- runit servers should be in /opt/container/services
  - if you have /etc/runit/runsvdir/default, move that to services
- use /opt/container/install.sh to install packages (ie not apt-get)

## Testing the base image
```bash
./build.sh
./run.sh &
docker exec --ti baseimage-ubuntu-XX /bin/bash
```
