FROM       ubuntu:20.04
MAINTAINER Arnold Hendriks <arnold@webhare.nl>
CMD        [ "/opt/container/launch.sh" ]
ENV        DEBIAN_FRONTEND=noninteractive

RUN        apt-get -qy update && \
           apt-get -qy upgrade && \
           apt-get -y install --no-install-recommends curl runit ca-certificates logrotate cron less vim gettext-base telnet iproute2 net-tools bind9-host iputils-ping && \
           ( apt-get changelog openssl | grep -q CVE-2021-3449 ) && \
           ( apt-get changelog openssl | grep -q CVE-2022-0778 ) && \
           apt-get -qy autoremove

ADD        dropins/ /
