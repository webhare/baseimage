#!/bin/bash
set -e # fail on first error
apt-get -q update
apt-get -qy install --no-install-recommends "$@"
apt-get -qy autoremove
